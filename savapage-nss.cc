/*
 * This file is part of the SavaPage project <https://www.savapage.org>.
 * Copyright (c) 2020 Datraverse B.V.
 * Author: Rijk Ravestein.
 *
 * SPDX-FileCopyrightText: © 2020 Datraverse B.V. <info@datraverse.com>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * For more information, please contact Datraverse B.V. at this
 * address: info@datraverse.com
 */

/**
 * @file savapage-nss.cc
 *
 * @brief Information provider about GNU/Linux users and groups
 * using the Name Service Switch, as configured in nsswitch.conf.
 */

//-------------------------------------
// C Library
//-------------------------------------
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <sys/types.h>

#include <pwd.h>
#include <grp.h>

//-------------------------------------
// Standard C++ Library
//-------------------------------------
#include <string>
#include <iostream>

#define CLI_OPT_USERS_DETAIL 			"--users"
#define CLI_OPT_GROUPS					"--user-groups"
#define CLI_OPT_GROUP_MEMBERS 	 		"--user-group-members"
#define CLI_OPT_IS_USER_GROUP			"--is-user-group"
#define CLI_OPT_IS_USER_GROUP_MEMBER	"--is-user-group-member"
#define CLI_OPT_USER_DETAILS 			"--user-details"
#define CLI_SWI_HELP					"--help"
#define CLI_SWI_HELP_SHORT				"-h"

//-------------------------------------
namespace org {
namespace savapage {
namespace nss {

static std::string program_version("1.2.0");
static std::string program_name;

static void ListUsers();
static void ListGroups();
static void ListGroupMembers(std::ostream &ostr, char *groupname);
static void IsUserInGroup(char *username, char *groupname);
static void GetUserDetails(char *username);
static void IsGroupPresent(char *groupname);
static void WriteBoolean(bool b);
static void WriteUserDetailsLine(std::ostream& ostr, char *username,
		char *realname);
/**
 * Parses the program name from the command-line invocation.
 */
static void ParseProgramName(const char* programPath) {

	const char * name;
	for (name = programPath + strlen(programPath);
			name != programPath && *name != '/'; name--)
		;
	if (*name == '/') {
		name++;
	}
	program_name = name;
}

/**
 * Writes program usage to stdout.
 */
static void PrintUsage() {
	printf(
			"____________________________________________________________________________\n"
					"SavaPage Name Service Switch Module v%s\n"
					"(c) 2020, Datraverse B.V.\n"
					"\n"
					"License: GNU AGPL version 3 or later <https://www.gnu.org/licenses/agpl.html>.\n"
					"         This is free software: you are free to change and redistribute it.\n"
					"         There is NO WARRANTY, to the extent permitted by law.\n"
					"\n"
					"Usage: %s [command] [option]...\n"
					"\n", program_version.c_str(), program_name.c_str());

	printf("%-40s %s\n", CLI_OPT_USERS_DETAIL,
			"Lists details of all users, one per line.");
	printf("%-40s %s\n", CLI_OPT_GROUPS,
			"Lists names of all user groups, one per line.");
	printf("%-40s %s\n", CLI_OPT_GROUP_MEMBERS" <group>",
			"Lists details about group members, one per line.");
	printf("%-40s %s\n", CLI_OPT_IS_USER_GROUP" <group>",
			"Echoes 'Y' if group is present, 'N' if not.");
	printf("%-40s %s\n", CLI_OPT_IS_USER_GROUP_MEMBER" <group> <user>",
			"Echoes 'Y' if user is member of group, 'N' if not.");
	printf("%-40s %s\n", CLI_OPT_USER_DETAILS" <user>",
			"Echoes user details on stdout.");
	printf("%-40s %s\n\n", CLI_SWI_HELP_SHORT "," CLI_SWI_HELP,
			"Shows this help text.");
}

}
}
}

/**
 *
 */
void org::savapage::nss::IsGroupPresent(char *groupname) {

	if (getgrnam(groupname) == NULL) {
		WriteBoolean(false);
	} else {
		WriteBoolean(true);
	}
}

/**
 *
 */
void org::savapage::nss::IsUserInGroup(char *groupname, char *username) {

	struct passwd *pw_record;
	struct group *grp_record;

	/*
	 * User must exist in /etc/passwd
	 */
	if ((pw_record = getpwnam(username)) == NULL) {
		// user does not exist
		WriteBoolean(false);
		return;
	}

	/*
	 * Check if there is a match with user's primary group.
	 */
	if ((grp_record = getgrgid(pw_record->pw_gid)) != NULL) {
		// this is a primary group
		if ((strcmp(groupname, grp_record->gr_name)) == 0) {
			// ... and the group matches
			WriteBoolean(true);
			return;
		}
	}

	/*
	 * Check group members and find match with user name.
	 */
	if ((grp_record = getgrnam(groupname)) == NULL) {
		// group does not exist
		WriteBoolean(false);
		return;
	}

	while (*(grp_record->gr_mem) != NULL) {
		if (strcmp(*((grp_record->gr_mem)++), username) == 0) {
			WriteBoolean(true);
			return;
		}
	}

	/*
	 * No match found.
	 */
	WriteBoolean(false);
}

/**
 *
 */
void org::savapage::nss::ListGroupMembers(std::ostream &ostr, char *groupname) {

	struct group *grp_record;
	struct passwd *pw_record;

	char *name = (char *) NULL;

	if ((grp_record = getgrnam(groupname)) == NULL) {
		throw std::string("Group does not exist.");
	}

	/*
	 * Get standard group members.
	 */
	while (*(grp_record->gr_mem) != NULL) {

		name = *(grp_record->gr_mem);

		/*
		 * Extract real name from passwd
		 */
		pw_record = getpwnam(name);

		if (pw_record != NULL && pw_record->pw_gecos != (char *) NULL) {
			ostr << name << "\t" << pw_record->pw_gecos << std::endl;
		} else {
			ostr << name << "\t" << std::endl;
		}

		grp_record->gr_mem++;
	}

	/*
	 * Traverse all users and check if their primary group match.
	 */
	while ((pw_record = getpwent()) != NULL) {

		if (grp_record->gr_gid == pw_record->pw_gid) {

			if (pw_record->pw_name != (char *) NULL) {

				if (pw_record->pw_gecos == (char *) NULL) {
					ostr << pw_record->pw_name << "\t" << std::endl;
				} else {
					ostr << pw_record->pw_name << "\t" << pw_record->pw_gecos
							<< std::endl;
				}

			}
		}
	}
}

/**
 *
 */
void org::savapage::nss::ListGroups() {

	struct group *grp_record;

	while ((grp_record = getgrent()) != NULL) {
		if (grp_record->gr_name != (char *) NULL) {
			std::cout << grp_record->gr_name << std::endl;
		}
	}
}

/**
 *
 */
void org::savapage::nss::WriteUserDetailsLine(std::ostream& ostr,
		char *username, char *realname) {
	ostr << username << "\t" << realname << std::endl;
}

/**
 *
 */
void org::savapage::nss::ListUsers() {

	struct passwd *pw_record;

	while ((pw_record = getpwent()) != NULL) {

		if (pw_record->pw_name != (char *) NULL
				&& pw_record->pw_gecos != (char *) NULL) {
			WriteUserDetailsLine(std::cout, pw_record->pw_name,
					pw_record->pw_gecos);
		}
	}
}

/**
 *
 */
void org::savapage::nss::GetUserDetails(char *username) {

	struct passwd *pw_record = getpwnam(username);

	if (pw_record == NULL) {
		throw std::string("User does not exist.");
	}

	if (pw_record != NULL && pw_record->pw_gecos != (char *) NULL) {
		WriteUserDetailsLine(std::cout, username, pw_record->pw_gecos);
	}
}

/**
 *
 */
void org::savapage::nss::WriteBoolean(bool b) {

	if (b) {
		std::cout << "Y";
	} else {
		std::cout << "N";
	}
	std::cout << std::endl;
}

/**
 *
 */
int main(int argc, char *argv[]) {

#ifdef PRODUCT_VERSION
	org::savapage::nss::program_version = PRODUCT_VERSION;
#endif

	org::savapage::nss::ParseProgramName(argv[0]);

	const char * cmd_option = argv[1];

	if (argc <= 1) {
		org::savapage::nss::PrintUsage();
		exit(EXIT_FAILURE);
	}

	try {

		if (strcmp(cmd_option, CLI_SWI_HELP_SHORT) == 0
				|| strcmp(cmd_option, CLI_SWI_HELP) == 0) {

			org::savapage::nss::PrintUsage();

		} else if (strcmp(cmd_option, CLI_OPT_USERS_DETAIL) == 0) {

			org::savapage::nss::ListUsers();

		} else if (strcmp(cmd_option, CLI_OPT_GROUPS) == 0) {

			org::savapage::nss::ListGroups();

		} else if (strcmp(cmd_option, CLI_OPT_GROUP_MEMBERS) == 0) {

			if (argc != 3) {
				throw std::string("Group name required.");
			}
			org::savapage::nss::ListGroupMembers(std::cout, argv[2]);

		} else if (strcmp(cmd_option, CLI_OPT_IS_USER_GROUP) == 0) {

			if (argc != 3) {
				throw std::string("Group name required.");
			}
			org::savapage::nss::IsGroupPresent(argv[2]);

		} else if (strcmp(cmd_option, CLI_OPT_IS_USER_GROUP_MEMBER) == 0) {

			if (argc != 4) {
				throw std::string("Group and user name required.");
			}
			org::savapage::nss::IsUserInGroup(argv[2], argv[3]);

		} else if (strcmp(cmd_option, CLI_OPT_USER_DETAILS) == 0) {

			if (argc != 3) {
				throw std::string("User name required.");
			}
			org::savapage::nss::GetUserDetails(argv[2]);

		} else {

			throw std::string("Unknown option.");
		}

	} catch (const std::string & error) {
		std::cerr << error << std::endl;
		exit(EXIT_FAILURE);
	}

	return (EXIT_SUCCESS);
}
